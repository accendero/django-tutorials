from django.apps import AppConfig


class AjaxWidgetConfig(AppConfig):
    name = 'ajax_widget'
