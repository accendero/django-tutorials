/**
 *
 * @param item: a single item from the user list
 * @param id: the numerical index of this item in the rendered list
 * @param field: the name of the html field (in this case 'selected_users')
 * @param target: the container for the created item
 */
function createUserOption(item, id, field, target) {
    var layout = $('<div>', {class: 'form-row field-' + field})
        .append($('<div>', {class: 'checkbox-row widget-ajax-' + field + '-checkbox'}));
    var checkbox_id = 'id_' + field + '_' + id.toString();
    var input = $('<input>', {
        type: 'checkbox',
        name: field,
        id: checkbox_id,
        checked: true,
        value: item.id
    }).css('display', true);
    var label = $('<label>', {
        for: checkbox_id,
        text: item.label,
        class: 'vCheckboxLabel'
    });
    layout.append(input);
    layout.append(label);
    $(target).append(layout);
}
