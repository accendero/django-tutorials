function ajaxSearch(e) {
    e.preventDefault();
    var url = $(this).data('widget-ajax-url');
    var field = $(this).data('field-name');
    var opt = window[$(this).data('option-function')];
    var post = {};
    var fieldName = this.name;
    post[fieldName] = $(this).val();
    var target = $(this).siblings('.widget-ajax-results');
    // remove existing selections
    $(target).empty();
    // add new options
    $.post(url, post, function(options) {
        options.forEach(function(option, idx) {
            opt(option, idx, field, target);
        });
    }, 'json');
}

function checkEnter(event) {
    if (event.key === 'Enter') {
        event.preventDefault();
        ajaxSearch.bind(this)(event);
    }
}

$(document).ready(function() {
    var search = $('.widget-ajax-search-text-field');
    search.on('blur', ajaxSearch);
    search.on('keydown', checkEnter);
});
