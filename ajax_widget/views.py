import json

from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render

from ajax_widget import forms


def get_user_list(request):
    if not request.is_ajax():
        return HttpResponse(status=404)
    query = request.POST.get('search_field')
    results = [
        {'id': u.id, 'label': u.username}
        for u in User.objects.filter(username__icontains=query)
    ]
    return HttpResponse(json.dumps(results))


def user_search_view(request):
    _form = forms.SelectUserForm
    form = _form()
    context = {}
    if request.method == 'POST':
        form = _form(request.POST)
        if form.is_valid():
            search = form.cleaned_data.get('search_field')
            selected_users = form.cleaned_data.get('selected_users')
            context.update({
                'search': search,
                'selected_users': selected_users,
            })
    context.update({'form': form})
    return render(request, 'select_user_form.html', context=context)
