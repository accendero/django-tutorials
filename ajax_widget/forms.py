from django import forms
from django.contrib.auth.models import User

from ajax_widget import widgets


class SelectUserForm(forms.Form):
    search_field = forms.CharField(
        widget=widgets.AjaxSearchListField(attrs={
            'data-field-name': 'selected_users',
            'data_field_name': 'selected_users',
            'data-option-function': 'createUserOption',
            'data_ajax_url': 'get_user_list',
            'widget_js': (
                'js/ajax/widgets/createUserOption.js',
            )
        }),
        required=False,
    )
    selected_users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        widget=forms.MultipleHiddenInput(),
    )
