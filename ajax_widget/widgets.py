from django import forms


class AjaxSearchListField(forms.widgets.TextInput):
    template_name = 'ajax_search_list.html'
    _widget_js = [
        'js/jquery.js',
        'js/ajax/init.js',
        'js/ajax/widgets/searchListField.js',
    ]

    def __init__(self, *args, **kwargs):
        attrs = kwargs.pop('attrs')
        # include any additional js for widget
        self._widget_js += attrs.pop('widget_js', [])
        kwargs['attrs'] = attrs
        super().__init__(*args, **kwargs)

    @property
    def media(self):
        return forms.Media(js=self._widget_js)
